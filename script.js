'use strict';



const modal = document.querySelector(".modal");
const overlay = document.querySelector(".overlay");
// Accessing "All" elements that has the "same className" using the "querySelectorAll() method".
const btnShowModal = document.querySelectorAll(".show-modal");
const btnCloseModal = document.querySelector(".close-modal");

// Event Handler function for Open Modal
const openModal = () => {
    modal.classList.remove("hidden");
    overlay.classList.remove("hidden");
}

// Event handlers Function for Closing the Modal
const closeModal = () => {
    modal.classList.add("hidden");
    overlay.classList.add("hidden");
}


// Show the Modal Window
// Accessing an array of btnShowModal
for(let i = 0; i < btnShowModal.length; i++){
    btnShowModal[i].addEventListener("click", openModal);
}

// Closing the Modal Window
btnCloseModal.addEventListener("click", closeModal); // Note in calling a function as a EventHandler dont add the Open & Close "()" because the function will happen when the event is clicked

// Closing the modal when clicked outside the modal window
overlay.addEventListener("click", closeModal);


// Keyboard Events are so called "GLobal Events || KeyPress Events"
document.addEventListener("keydown", e => {
    // To check the event that is pressed in the keyboard
    // to call the specific keyboard event use the ".key" property
    console.log(e.key);

    // if "Escape" key is pressed & modal is "Active"
    if(e.key === "Escape" && !modal.classList.contains("hidden")){ 
        closeModal(); // Then Call the closeModal Function
    }
});